/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;


/**
 *
 * @author Lorenzo
 */
public class TestCaseCompraPasajesTest {
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();

    public TestCaseCompraPasajesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Lorenzo\\Desktop\\chromedriver_win32\\chromedriver.exe");
        driver = new ChromeDriver();
        baseUrl = "http://newtours.demoaut.com/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }
    
    @After
    public void tearDown() {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
          fail(verificationErrorString);
        }
    }
    
    @Test
    public void testCaseCompraPasajes() throws Exception {
        driver.get(baseUrl + "/");
        driver.findElement(By.name("userName")).clear();
        driver.findElement(By.name("userName")).sendKeys("tutorial");
        driver.findElement(By.name("password")).clear();
        driver.findElement(By.name("password")).sendKeys("tutorial");
        driver.findElement(By.name("login")).click();
        try {
          assertTrue(isElementPresent(By.linkText("SIGN-OFF")));
        } catch (Error e) {
          verificationErrors.append(e.toString());
        }
        new Select(driver.findElement(By.name("fromPort"))).selectByVisibleText("London");
        new Select(driver.findElement(By.name("toPort"))).selectByVisibleText("Paris");
        // ERROR: Caught exception [Error: Dom locators are not implemented yet!]
        driver.findElement(By.cssSelector("input:nth-child(4)")).click();
        driver.findElement(By.name("findFlights")).click();
        driver.findElement(By.name("reserveFlights")).click();
        driver.findElement(By.name("passFirst0")).clear();
        driver.findElement(By.name("passFirst0")).sendKeys("Juan");
        driver.findElement(By.name("passLast0")).clear();
        driver.findElement(By.name("passLast0")).sendKeys("Perez");
        driver.findElement(By.name("creditnumber")).clear();
        driver.findElement(By.name("creditnumber")).sendKeys("12345678");
        driver.findElement(By.name("buyFlights")).click();
        driver.findElement(By.linkText("SIGN-OFF")).click();
    }
    
    private boolean isElementPresent(By by) {
        try {
          driver.findElement(by);
          return true;
        } catch (NoSuchElementException e) {
          return false;
        }
    }

    private boolean isAlertPresent() {
        try {
          driver.switchTo().alert();
          return true;
        } catch (NoAlertPresentException e) {
          return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
          Alert alert = driver.switchTo().alert();
          String alertText = alert.getText();
          if (acceptNextAlert) {
            alert.accept();
          } else {
            alert.dismiss();
          }
          return alertText;
        } finally {
          acceptNextAlert = true;
        }
    }
}
